###############################################################################################################
# PARAMETRES :
# $1 => Token avec droit API (role =>Reporter)
# $2 => Url git des api des projets
# $3 => Numero d'id du projet gitlab
#
# CODE DE RETOUR :
# 0 => L'exécution est réussi
# 1 => Erreur durant l'exécution
###############################################################################################################

from git import Repo
import os
import git
import sys
import re
import requests

class CGitlabVerifIssues:
    """
    class permettant de realiser des verfications d'issues gitlab 
    lors de merge request et de leurs existance dans l'API gitlab.

    ...

    Attribus
    ----------
    api_key : str
        Token avec droit API (role =>Reporter).
    url_projet_api : str
        Url git des api des projets.
    id_projet : int
        Numero d'id du projet gitlab.
    list_issues : array
        Liste des issues trouvaient dans le titre de la merge request.
    headers : set
        Contient l'entete de la requete avec le token api utilisé 
        lors d'une requete vers l'api gitlab.

    Méthodes
    -------
    Check_issue_merge():
        Vérifie la presence au minimum d'un numéro d'issue 
        dans le titre de la merge request.
    
    Check_issue_exist():
        Vérifie si toutes les issues existent bien dans l'api gitlab du projet.
    """
    

    def __init__(self,
                 api_key,
                 url_projet_api,
                 id_projet):
        """
        Initialisation des attributs.

            Parametres:
                    api_key (int): Token avec droit API (role =>Reporter)
                    url_projet_api (int): Url git des api des projets
                    id_projet (int): Numero d'id du projet gitlab

        """

        self.api_key=api_key
        self.id_projet=id_projet
        self.list_issues=[]
        self.headers={'PRIVATE-TOKEN': self.api_key}
        self.url_projet_api=url_projet_api



    def Check_issue_merge(self):
        """
        Vérifie la presence au minimum d'un numéro d'issue dans le titre de la merge request.

            Retour:
                    0 (int): L'exécution est réussi
                    1 (int): Erreur durant l'exécution
        """

        regex = re.compile("#\\d+")
        
        url = self.url_projet_api + self.id_projet + "/merge_requests?state=opened"
        print(url)
        resp = requests.get(url,headers=self.headers)
        if (resp.status_code == requests.codes.ok):
            list_mr=resp.json()
            for mr in list_mr:
                title=mr["title"]
                self.list_issues = regex.findall(title)
                if (0 < len(self.list_issues)):
                    print(F"Merge request title : {title}")
                    return 0
                else:
                    print(F"Error : Issue not find in Title Merge Request !!!")
                    print(F"Merge request title : {title}")
                    return 1
        else:
            print(F"Error : When call url = ({url})")
            return 1
        return 0

    def Check_issue_exist(self):
        """
        Vérifie si toutes les issues existent bien dans l'api gitlab du projet.

            Retour:
                    0 (int): L'exécution est réussi
                    1 (int): Erreur durant l'exécution
        """
        
        for issue in ( x[1:] for x in self.list_issues ):
            url = self.url_projet_api + self.id_projet + "/issues/" + issue
            print(url)
            resp = requests.get(url,headers=self.headers)
            if (resp.status_code != requests.codes.ok):
                print( F"Error : Issue N° ({issue}) not exist !!!")
                return 1
            print( F"Issue N° ({issue}) find")    
        return 0

VerifIssues = CGitlabVerifIssues(sys.argv[1],
                                 sys.argv[2],
                                 sys.argv[3])
ret=VerifIssues.Check_issue_merge()
if(ret == 0):
    ret=VerifIssues.Check_issue_exist()
    if(ret == 0):
        sys.exit(0)
    else:
        sys.exit(1)
else:
    sys.exit(1)

